<?php

namespace CPTeam\Model {
	
	interface IUserRepository
	{
		/**
		 * @param $id
		 *
		 * @return IUser
		 */
		public function getById($id);
	}
	
	/**
	 * Interface IUser
	 *
	 * @package CPTeam\Nextras\Orm
	 *
	 * @property $id
	 * @property $role
	 */
	interface IUser
	{
		/** @return array */
		public function roleToArray();
	}
}

namespace CPTeam\Security {
	
	interface User
	{
		public function getId();
	}
}


namespace CPTeam\Roles {

	interface Role
	{
		const ADMIN = "admin";
		const USER = "user";
		const GUEST = "guest";
		const COSPLAYER = "cosplayer";
	}

	interface Guest extends Role
	{

	}

	interface User extends Role
	{

	}

	interface Cosplayer extends User
	{

	}

	interface Admin extends User
	{

	}

}
