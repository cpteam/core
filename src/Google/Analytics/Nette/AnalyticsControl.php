<?php
/**
 * Created by PhpStorm.
 * User: lukys
 * Date: 25.8.16
 * Time: 23:51
 */

namespace CPTeam\Google\Analytics\Nette;


use CPTeam\Google\Analytics\AnalyticsParser;
use Nette\Application\UI\Control;

/**
 * Class AnalyticsControl
 *
 * @package app\components\Google\Analytics
 */
class AnalyticsControl extends Control
{
    /** @var  AnalyticsParser */
    private $analyticsParser;

    /**
     * AnalyticsControl constructor.
     *
     * @param AnalyticsParser $analyticsParser
     */
    public function __construct(AnalyticsParser $analyticsParser)
    {
        $this->analyticsParser = $analyticsParser;
    }


    public function render()
    {

        $this->template->parser = $this->analyticsParser;

        $this->template->setFile(__DIR__ . "/analytics.latte");
        $this->template->render();
    }

}

