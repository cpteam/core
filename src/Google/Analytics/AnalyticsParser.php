<?php
/**
 * Created by PhpStorm.
 * User: lukys
 * Date: 25.8.16
 * Time: 23:51
 */

namespace CPTeam\Google\Analytics;


use CPTeam\CoreException;
use CPTeam\RuntimeException;
use Nextras\Dbal\Utils\DateTime;

class AnalyticsParser
{
    /** @var \Google_Service_AnalyticsReporting  */
    private $analytics;

    /** @var  int */
    private $profile;

    /** @var   */
    private $results;

    public function __construct($config)
    {

        $client = new \Google_Client();
        $client->setApplicationName("Hello Analytics Reporting");
        $client->setAuthConfig($config);
        $client->setScopes(\Google_Service_Analytics::ANALYTICS_READONLY);

        $this->analytics = new \Google_Service_Analytics($client);


        $analytics = $this->analytics;
        // Get the user's first view (profile) ID.

        // Get the list of accounts for the authorized user.
        $accounts = $analytics->management_accounts->listManagementAccounts();

        if (count($accounts->getItems()) > 0) {
            $items = $accounts->getItems();
            $firstAccountId = $items[0]->getId();

            // Get the list of properties for the authorized user.
            $properties = $analytics->management_webproperties
                ->listManagementWebproperties($firstAccountId);

            if (count($properties->getItems()) > 0) {
                $items = $properties->getItems();
                $firstPropertyId = $items[0]->getId();

                // Get the list of views (profiles) for the authorized user.
                $profiles = $analytics->management_profiles
                    ->listManagementProfiles($firstAccountId, $firstPropertyId);

                if (count($profiles->getItems()) > 0) {
                    $items = $profiles->getItems();

                    // Return the first view (profile) ID.
                    $this->profile = $items[0]->getId();

                } else {
                    throw new AnalyticsException('No views (profiles) found for this user.');
                }
            } else {
                throw new AnalyticsException('No properties found for this user.');
            }
        } else {
            throw new AnalyticsException('No accounts found for this user.');
        }


        $analytics = $this->analytics;
        $profileId = $this->profile;

        // Calls the Core Reporting API and queries for the number of sessions
        // for the last seven days.
        $this->results = $analytics->data_ga->get(
            'ga:' . $profileId,
            '30daysAgo',
            'today',
            'ga:avgSessionDuration');
    }


    function printResults() {
        $results = $this->results;

        if (count($results->getRows()) > 0) {

            $sec = $results->getRows()[0][0];

            $dt = new DateTime();
            $dt->setTime(0, 0, 0);
            $dt->setTimestamp($dt->getTimestamp() + (int)$sec);

            return $dt->format('H:i:s');
        }
    }
}

class AnalyticsException extends RuntimeException {
    
}
