<?php
namespace CPTeam\Date;

/**
 * Class ConvertorException
 *
 * @package CPTeam\Date
 */
class ConvertorException extends \Exception
{
}

/**
 * Class OutOfBoundsException
 *
 * @package CPTeam\Date
 */
class OutOfBoundsException extends ConvertorException
{
}

/**
 * Class InvalidFormatException
 *
 * @package CPTeam\Date
 */
class InvalidFormatException extends ConvertorException
{
}

/**
 * Class Convertor
 *
 * @package CPTeam\Date
 */
class Convertor
{
    /**
     * @var string Regex for validation of classic date
     * @link https://regex101.com/r/jJ3zF2/3
     */
    const TO_EXT_PATTERN = '#^(?P<year>[0-9]{4})((?P<del>[.\-\/])(?P<month>[0-1][0-9]))?((\k<del>)(?P<day>[0-3][0-9]))?$#';

    /**
     * @var string Regex for validation extended date
     */
    const FROM_EXT_PATTERN = '#^(?P<year>[0-9]{4})(?P<month>([0-1][0-9])|00)(?P<day>([0-3][0-9])|00)$#';

    /**
     * @var string Padding in extended date
     * @link https://regex101.com/r/bQ5hF1/1
     */
    const PADDING = '00';


    /**
     * @param string $date Date in Y<delimiter>m<delimiter>d, Y<delimiter>m, Y format
     *
     * @return string Formatted string, format is YYYY0000 or YYYYMM00 or YYYYMMDD
     *
     * @throws InvalidFormatException
     * @throws OutOfBoundsException
     */
    public static function toExt($date)
    {
        $matches = [];

        if (preg_match(self::TO_EXT_PATTERN, $date, $matches)) {
            $result = '';
            $day = isset($matches['day']) ? $matches['day'] : null;
            $month = isset($matches['month']) ? $matches['month'] : null;
            $year = $matches['year']; //year is always set

            if ($day === null) {
                if ($month === null) {
                    $result = $year . self::PADDING . self::PADDING;
                } else {
                    if ($month > 12) throw new OutOfBoundsException("month: $month is out of bounds");
                    $result = $year . $month . self::PADDING;
                }
            } else {
                //month is also set
                $result = $year . $month . $day;
                //verify date
                if (checkdate($month, $day, $year) === false) throw new OutOfBoundsException("day: $day or year: $year or month: $month is out of bounds");
            }

            return $result;
        } else {
            throw new InvalidFormatException("Input: $date");
        }
    }
    
    
    /**
     * @param string $extended Formatted string, format is YYYY0000 or YYYYMM00 or YYYYMMDD (@see CPTeam\Date\Convertor::toExt())
     *
     * @return \stdClass Standard class with month, day and year properties (set to null by default)
     *
     * @throws InvalidFormatException
     * @throws OutOfBoundsException
     */
    public static function fromExt($extended)
    {
        $matches = [];
        if (preg_match(self::FROM_EXT_PATTERN, $extended, $matches)) {
            $year = $matches['year'];
            $month = $matches['month'];
            $day = $matches['day'];

            $result = new \stdClass();
            $result->day = null;
            $result->month = null;
            $result->year = null;

            if ($month === self::PADDING) {
                //day is also padded/empty
                $result->year = $year;
            } else {
                if ($day === self::PADDING) {
                    if ($month > 12) throw new OutOfBoundsException("month: $month is out of bounds");
                    $result->year = $year;
                    $result->month = $month;
                } else {
                    //verify date
                    if (checkdate($month, $day, $year) === false) throw new OutOfBoundsException("day: $day or year: $year or month: $month is out of bounds");
                    $result->year = $year;
                    $result->month = $month;
                    $result->day = $day;
                }
            }
            return $result;
        } else {
            throw new InvalidFormatException("Input: $extended");
        }
    }
}
