<?php
/**
 * Created by PhpStorm.
 * User: luky
 * Date: 13.7.16
 * Time: 19:32
 */

namespace CPTeam;


/**
 * Class EnchancedCollection
 *
 * @package CPTeam
 */
class EnchancedCollection
{
	public function __construct()
	{
		throw new StaticClassException('Static class can\'t be initialized');
	}

	public static function fetchMultiplePairs($data, $fetch)
	{
		$arr = [];

		foreach ($data as $item) {
			$innerArr = [];

			foreach ($fetch[1] as $colName => $col) {

				if(substr($col, 0, 6) == "this->") {
					$col = substr($col, 6);
					$col = explode("->", $col);

					$x = $item;
					
	
					foreach ($col as $c) {
						$x = $x->$c;
						
						if(!$x) {
							break;
						}
						
					}
					
					$innerArr[$colName] = $x;
					
				} else {
					$innerArr[$colName] = $item->$col;
				}
			}

			if ($fetch[0]) {
				$arr[$item->{$fetch[0]}] = $innerArr;
			} else {
				$arr[] = $innerArr;
			}
			
		}
		
		return $arr;
	}
	

}