<?php

namespace CPTeam\Tracy\Helpers;

use Tracy;

/**
 * Class BlueScreen
 *
 * @package CPTeam\Tracy\Helpers
 */
class BlueScreen
{
	
	public static function addExceptionPreview($exception, $panelName, $file, $line)
	{
		$blueScreen = Tracy\Debugger::getBlueScreen();
		$blueScreen->addPanel(function ($e) use ($exception, $panelName, $file, $line) {
			if ($e instanceof $exception) {
				return [
					'tab' => $panelName,
					'panel' => '<p><strong>File:</strong> ' . Tracy\Helpers::editorLink($file, $line) . '</p>'
						. Tracy\BlueScreen::highlightFile($file, $line),
				];
			}
			
			return [];
		});
	}
	
}
