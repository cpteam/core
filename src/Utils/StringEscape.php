<?php

namespace CPTeam\Utils;

/**
 * Class String Escape
 *
 * @author  Lukas "Arzi" Klima
 * @version 0.2
 * @license MIT
 */
class StringEscape
{
	const LENGTH_SHORT = 520;

	/** Escape Js, divneho paragrafu, whitespaces, double enter, make url, make <br>
	 *
	 * @param $text
	 * @return mixed|string
	 * @deprecated
	 */
	public static function basic($text, $lenght = self::LENGTH_SHORT)
	{
		$text = self::escapeHtml($text);

		if ($lenght == false) {
			$text = self::short($text, $lenght);
		}

		$text = self::char($text);
		$text = self::trim($text);

		$text = str_ireplace("\n", "\n<br>", $text);

		return $text;
	}

	public static function escapeHtml($text)
	{
		return htmlspecialchars($text, ENT_COMPAT | ENT_HTML401, 'ISO-8859-1');
	}

	public static function short($text, $length = self::LENGTH_SHORT)
	{
		$textLenght = mb_strlen($text);

		if ($textLenght < $length) {
			return $text;
		}

		$space = false;
		for ($i = $length; $i >= 0; $i--) {
			if (in_array(mb_substr($text, $i, 2), [". ", "! ", "? ", " ", "\n"])) {
				return mb_substr($text, 0, $i + 1) . " <a href='' onClick='$(this).next().show(); $(this).remove();return false;'>Zobrazit vše</a><div style='display: none'>" . mb_substr($text, $i + 2) . "</div>";
			}
		}
	}

	/**
	 * @param $text
	 * @return mixed
	 */
	public static function char($text)
	{
		$a = [
			'`' => "'",
		];
		foreach ($a as $b => $c) {
			$text = str_replace($b, $c, $text);
		}

		return $text;
	}


	public static function trim($text)
	{
		$text = trim($text);

		return self::doubleLine($text);
	}

	private static function doubleLine($text)
	{
		return preg_replace("/^(\s*(\r\n|\n|\r)){2,}/m", "\n", $text);
	}

	/** @deprecated */
	public static function toDatabase($text)
	{
		$text = self::escapeHtml($text);
		$text = self::char($text);

		$text = self::trim($text);

		return $text;
	}

	public static function preview($text, $length = self::LENGTH_SHORT)
	{
		$textLenght = mb_strlen($text);

		if ($textLenght < $length) {
			return $text;
		}
		$space = false;
		for ($i = $length; $i >= 0; $i--) {
			if (mb_substr($text, $i, 1) == " ") {
				$space = true;
			} elseif ($space == true && (mb_substr($text, $i, 1) == "." || mb_substr($text, $i, 1) == "!" || mb_substr($text, $i, 1) == "?")) {
				return mb_substr($text, 0, $i + 1) . " [...]";
			} else {
				$space = false;
			}
		}
	}


	public static function fromTextarea($text)
	{

		$text = self::escapeHtml($text);

		$text = self::char($text);
		$text = self::trim($text);

		$text = self::url($text);
		$text = self::createList($text);

		return str_ireplace("\n", "<br />", $text);
	}

	/**
	 * @param $url
	 * @return mixed
	 */
	public static function url($url)
	{
		$apostrof = "'";
		$reg_exUrl = ('/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;"' . $apostrof . '()]*[-a-z0-9+&@#\/%=~"_|()]/im');

		$url = preg_replace('/^\b((?!(?:https?|ftp):\/\/)(www\.))/im', 'http://www.', $url);

		return preg_replace($reg_exUrl, '<i><a href="$0" target="_blank" rel="nofollow">$0</a></i>', $url);
	}

	public static function createList($text)
	{
		$text = preg_replace("/\*+(.*)?/i", "<ul><li>$1</li></ul>", $text);
		$text = preg_replace("/(\<\/ul\>\n(.*)\<ul\>*)+/", "", $text);

		return $text;
	}

	public static function seo($data)
	{
		$transfer = ["á" => "a", "ä" => "a", "č" => "c", "ď" => "d", "é" => "e", "ě" => "e", "ë" => "e", "í" => "i", "&#239;" => "i", "ň" => "n", "ó" => "o", "ō" => "ou", "Ō" => "Oo", "ö" => "o", "ř" => "r", "š" => "s", "ť" => "t", "ú" => "u", "ů" => "u", "ü" => "u", "ý" => "y", "&#255;" => "y", "ž" => "z", "Á" => "A", "Ä" => "A", "Č" => "C", "Ď" => "D", "É" => "E", "Ě" => "E", "Ë" => "E", "Í" => "I", "&#207;" => "I", "Ň" => "N", "Ó" => "O", "Ö" => "O", "Ř" => "R", "Š" => "S", "Ť" => "T", "Ú" => "U", "Ů" => "U", "Ü" => "U", "Ý" => "Y", "&#376;" => "Y", "Ž" => "Z"];
		$data = strtr($data, $transfer);
		$data = strtolower($data);
		$data = preg_replace("/[^[:alpha:][:digit:]]/", "-", $data);
		$data = trim($data, "-");
		$data = preg_replace("/[-]+/", "-", $data);

		return $data;
	}

	/**
	 * @param $url
	 * @return mixed
	 */
	public static function getYoutubeUrl($text)
	{
		if (preg_match('/youtube\.com\/watch\?v=([^\&\?\/]+)/', $text, $id)) {
			return $id[1];
		} else if (preg_match('/youtube\.com\/embed\/([^\&\?\/]+)/', $text, $id)) {
			return $id[1];
		} else if (preg_match('/youtube\.com\/v\/([^\&\?\/]+)/', $text, $id)) {
			return $id[1];
		} else if (preg_match('/youtu\.be\/([^\&\?\/]+)/', $text, $id)) {
			return $id[1];
		} else if (preg_match('/youtube\.com\/verify_age\?next_url=\/watch%3Fv%3D([^\&\?\/]+)/', $text, $id)) {
			return $id[1];
		}

		return false;
	}


	public static $rules = [
		'/(#+)(.*)/' => 'self::header',                           // headers
		/*	'/\[([^\[]+)\]\(([^\)]+)\)/' => '<a href=\'\2\'>\1</a>',  // links
			'/(\*\*|__)(.*?)\1/' => '<strong>\2</strong>',            // bold
			'/(\*|_)(.*?)\1/' => '<em>\2</em>',                       // emphasis
			'/\~\~(.*?)\~\~/' => '<del>\1</del>',                     // del
			'/\:\"(.*?)\"\:/' => '<q>\1</q>',                         // quote
			'/`(.*?)`/' => '<code>\1</code>',                         // inline code
			'/\n\*(.*)/' => 'self::ul_list',                          // ul lists
			'/\n[0-9]+\.(.*)/' => 'self::ol_list',                    // ol lists
			'/\n(&gt;|\>)(.*)/' => 'self::blockquote ',               // blockquotes
			'/\n-{5,}/' => "\n<hr />",                                // horizontal rule
			'/\n([^\n]+)\n/' => 'self::para',                         // add paragraphs
			'/<\/ul>\s?<ul>/' => '',                                  // fix extra ul
			'/<\/ol>\s?<ol>/' => '',                                  // fix extra ol
			'/<\/blockquote><blockquote>/' => "\n"                    // fix extra blockquote
		*/

	];

	public static function markDown($text)
	{
		foreach (self::$rules as $regex => $replacement) {
			if (is_callable($replacement)) {
				$text = preg_replace_callback($regex, $replacement, $text);
			} else {
				$text = preg_replace($regex, $replacement, $text);
			}
		}

		return $text;
	}

	private static function header($regs)
	{
		list ($tmp, $chars, $header) = $regs;
		$level = strlen($chars);

		return sprintf('<h%d>%s</h%d>', $level, trim($header), $level);
	}


}
