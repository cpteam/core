<?php

namespace CPTeam\Google\Analytics\Nette;


/**
 * Interface IAnalyticsFactory
 *
 * @package app\components\Google\Analyticss
 */
interface IAnalyticsFactory
{
    /** @return AnalyticsControl */
    public function create();
}
