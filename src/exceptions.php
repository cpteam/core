<?php

namespace CPTeam;

//Logic exception (For developers - argument must be integer...)

/**
 * Class LogicException
 */
class LogicException extends \LogicException {}

/**
 * Class InvalidArgumentException
 */
class InvalidArgumentException extends LogicException {}

/**
 * Class NotImplementedException
 */
class NotImplementedException extends LogicException {}

/**
 * Class StaticClassException
 */
class StaticClassException extends LogicException {}

// Runtime (For application fails)

/**
 * Class RuntimeException
 */
class RuntimeException extends \RuntimeException {}

/**
 * Class IOException
 */
class IOException extends RuntimeException {}

/**
 * Class FileNotFoundException
 */
class FileNotFoundException extends IOException {}

/**
 * Class DirectoryNotFoundException
 */
class DirectoryNotFoundException extends IOException {}

/**
 * Class EntryNotFoundException
 */
class EntryNotFoundException extends RuntimeException {}

/**
 * Class DuplicateEntryException
 */
class DuplicateEntryException extends RuntimeException {}











