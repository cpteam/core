<?php

namespace CPTeam\Utils;

use CPTeam\StaticClassException;

class Autoloader
{
	private static $dir = [];
	
	public static function registerDir($dir, $recursive = true)
	{
		if ($recursive) {
			self::scanDir($dir);
		} else {
			self::$dir[] = $dir;
		}
	}
	
	private static function scanDir($path)
	{
		foreach (scandir($path) as $value) {
			if ($value == "." || $value == "..") {
				continue;
			}
			
			$file = realpath($path . DIRECTORY_SEPARATOR . $value);
			
			if (is_dir($file)) {
				self::$dir[] = $file;
				self::scanDir($file);
			}
		}
	}
	
	public static function load()
	{
		
		spl_autoload_register(function ($class) {
			$class = explode("\\", $class);
			
			$class = end($class);
			
			foreach (self::$dir as $dir) {
				if (file_exists($dir . "/" . $class . ".php")) {
					require_once($dir . "/" . $class . ".php");
					break;
				}
			}
		});
	}
	
	/**
	 * Autoloader constructor.
	 */
	public function __construct()
	{
		throw new StaticClassException('Autoloader can\'t be inicialized.');
	}
	
}



